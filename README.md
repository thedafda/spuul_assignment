###Spuul Assignment

[ ![Codeship Status for thedafda/spuul_assignment](https://codeship.com/projects/24202740-dcd1-0133-ec38-2682daabcfd0/status?branch=master)](https://codeship.com/projects/144203)

Prod Deployment : https://s3-ap-southeast-1.amazonaws.com/spuul.assigment/index.html

## Setup
	1. Install NodeJS
	2. Install global `grunt` NPM dependecies using `npm install -g grunt-cli`
	3. Start Development server using `npm start` or 'grunt'
	4. To create prod version use `grunt build-prod` command this will generate minified and productionr ready code inde `/dist` directory

## Deployment 
	Deployment is done using codeship and directly dumped all prod static bundle files to s3 bucket