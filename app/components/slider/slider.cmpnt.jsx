import React, {Component, PropTypes, cloneElement} from 'react';

export default class Slider extends Component {

    constructor(props) {
        super(props);
        this.state = {
            slideTrack: this.props.startIndex || 0
        };
    }

    goNextSlide() {
        if(this.inGoing) {
            return;
        }
        if(this.state.slideTrack >= this.props.children.length - 1  && !this.props.loop) {
            return;
        }
        if(this.props.loop) {
            this.setState({
                going: 'next'
            });
            this.inGoing = true;
            setTimeout(() => {
                this.inGoing = false;
                this.setState({
                    going: 'none',
                    slideTrack: this.state.slideTrack + 1
                });
            }, 500);
            return;
        }
        this.setState({
            slideTrack: this.state.slideTrack + 1
        });
    }

    goPrevSlide() {
        if(this.inGoing) {
            return;
        }
        if(this.state.slideTrack <= 0 && !this.props.loop){
            return;
        }
        if(this.props.loop) {
            this.setState({
                going: 'back'
            });
            this.inGoing = true;
            setTimeout(() => {
                this.inGoing = false;
                this.setState({
                    going: 'none',
                    slideTrack: this.state.slideTrack - 1
                });
            }, 500);
            return;
        }
        this.setState({
            slideTrack: this.state.slideTrack - 1
        });
    }

    getSingleChild(rEle, key, idx){
        let props = {};
        if(key) {
            props.key = key;
        }
        let className = (rEle.props.className || '');
        if(this.props.selectedTabIndex === idx) {
            className = className + ' active'; 
        }
        Object.assign(props, rEle.props, {
           className: className + ' slide'
        });
        return cloneElement(rEle, props);
    }

    getChildrens() {
        return this.props.children && this.props.children.map((rEle, idx) => {
            return this.getSingleChild(rEle, undefined, idx);
        });
    }

    getTheChosenFive(index) {
        const allChilds = this.props.children || [];
        let outChilds = [];
        for(let i = index - 2; i <= index + 2; i++) {
            let p = i % allChilds.length;
            if(p < 0) {
                p = allChilds.length + p;
            }
            if(allChilds[p] !== undefined) {
                let child = this.getSingleChild(allChilds[p], i + '');
                outChilds.push(child);    
            }
        }
        return outChilds;
    }

    render() {
        let childrens;
        let sliderStyle = {};
        let prevBtnclassNames = "prev";
        let nextBtnclassNames = "next";
        if(this.props.loop) {
            childrens = this.getTheChosenFive(this.state.slideTrack);
            let num = -200;
            if(this.state.going === 'next') {
                num = -300;
            } else if (this.state.going === 'back') {
                num = -100;
            }
            sliderStyle = {
                transform: `translateX(${num + "%"})`
            };
            if(this.state.going !== 'none') {
                sliderStyle.transition =  "transform .4s";
            }
        } else {
            childrens = this.getChildrens();
            sliderStyle = {
                transition: "transform .4s",
                transform: `translateX(${(-this.state.slideTrack * 100) + "%"})`
            };
            if(this.state.slideTrack === childrens.length - 1) {
                nextBtnclassNames = nextBtnclassNames + ' hide';
            } else if(this.state.slideTrack <= 0) {
                prevBtnclassNames = prevBtnclassNames + ' hide';
            }
        }
        let sliderCntnrClassName = 'slider-cntnr' + (this.props.threeSlideMode ? ' three-slide-mode' : '');
        return (
            <div className="slider-main-cntnr">
                <div className={sliderCntnrClassName}>
                    <div className="slider" ref="slider" style={sliderStyle}>
                        {childrens}
                    </div>
                </div>
                <div className="controls">
                    <button className={prevBtnclassNames} onClick={this.goPrevSlide.bind(this)}> &lt; </button>
                    <button className={nextBtnclassNames} onClick={this.goNextSlide.bind(this)}> &gt; </button>
                </div>
            </div>
        );
    }
}
