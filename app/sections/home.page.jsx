import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Slider from '../components/slider/slider.cmpnt';
import ImageService from '../service/image.service';

export default class HomeSection extends Component {

    constructor(props) {
        super(props)
        this.state = {
            imageSlides: [{
                uniqId: 1,
                image: 'imgs/lena.jpg'
            }, {
                uniqId: 2,
                image: 'imgs/pattern.jpg'
            }, {
                uniqId: 3,
                image: 'imgs/processing.jpg'
            }, {
                uniqId: 4,
                image: 'imgs/unnamed.png'
            }],
            textSlides: [
            	"Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            	"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
            	"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', comes from a line in section 1.10.32.",
            	"The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.",
            	"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassi"
            ],
            ajaxSlides: []
        };
    }

    componentDidMount(){
        this.loadAjaxImages();
    }

    getImageSlides() {
    	return this.state.imageSlides && this.state.imageSlides.map((slide, idx) => {
           	return (
           		<div key={idx + slide.uniqId} className="image-tile">
           			<img src={slide.image} />
           		</div>
           	);
        });
    }

    getTextSlides() {
    	return this.state.textSlides && this.state.textSlides.map((text, idx) => {
           	return (
           		<div key={idx} className="text-tile">
           			{text}
           		</div>
           	);
        });
    }

    getAjaxImagesSlides() {
        return this.state.ajaxSlides && this.state.ajaxSlides.map((o, idx) => {
            const resl = ImageService.getImagesResoutions();
            let aTags = resl.map((x) => {
                let style = {
                     backgroundImage: 'url("' + o.image[x] + '")',
                };
                return (
                    <a key={x} href={o.link} className={x} style={style}></a>
                );
            });
            
            return (
                <div key={o.id} className="ajax-tile">
                    {aTags}
                </div>
            );
        });
    }

    loadAjaxImages(){
        return ImageService
            .getImagesFromServer()
            .then((data) => {
                this.setState({
                    ajaxSlides: data
                });
            });
    }

    render() {
    	let imageSlides = this.getImageSlides();
    	let textSlides = this.getTextSlides();
        let ajaxSlides = this.getAjaxImagesSlides() || [];
        return (
        	<div>
                <div className="ajax-slider">
                    <h3>AJAX Slider</h3>
                    {ajaxSlides.length ? (
                        <Slider loop={true}>
                            {ajaxSlides}
                        </Slider>
                    ) : (
                        <button onClick={this.loadAjaxImages.bind(this)}>
                            Click to load data from server
                        </button>
                    )}
                </div>

                <div className="ajax-slider">
                    <h3>AJAX Slider With Three Slide Mode</h3>
                    {ajaxSlides.length ? (
                        <Slider loop={true} threeSlideMode={true}>
                            {ajaxSlides}
                        </Slider>
                    ) : (
                        <button onClick={this.loadAjaxImages.bind(this)}>
                            Click to load data from server
                        </button>
                    )}
                </div>

                <hr/>
	        	<div className="image-slider" >
                    <h3>Image Slider</h3>
	            	<Slider loop={true}>
	            		{imageSlides}
	            	</Slider>
	            </div>
	            
	            <hr/>
	            <div className="text-slider">
                    <h3>Text Slider</h3>
	            	<Slider>
	            		{textSlides}
	            	</Slider>
	            </div>
                
            </div>
        );
    }
}



// Document Ready
document.addEventListener('DOMContentLoaded', function() {
    ReactDOM.render( <HomeSection/> , document.getElementById('home-page'));
});
