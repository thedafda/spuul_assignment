import $ from "jquery";

const URLS = {
	IMAGE: 'https://api.myjson.com/bins/1wqfa'
};

export default class ImageService{

    constructor(props) {
    	
    }

    static getImagesFromServer() {
    	return $
    		.get(URLS.IMAGE)
    		.then((data) => {
    			console.log(data);
    			return data;
    		});
    }

    static getImagesResoutions(){
        return ["large", "medium", "small", "xsmall"];
    }

}
