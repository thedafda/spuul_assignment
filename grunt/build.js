'use strict';

module.exports = function(grunt) {

    grunt.task.registerTask('default', ['connect:dev', 'watch']);

    grunt.task.registerTask('build', ['connect:dev', 'watch']);

    grunt.task.registerTask('build-prod', [
    	'clean:prod',
    	'less:prod',
    	'webpack:prod',
    	'copy:prodImage',
    	'copy:prodIndex'
    ]);
};
