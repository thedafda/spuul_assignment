'use strict';

module.exports = function(grunt) {
    return {
        dev: {
            options: {
                port: 9090,
                base: 'app'
            }
        },
        prod: {
            options: {
                port: 9091,
                base: 'dist'
            }
        }
    };
};
