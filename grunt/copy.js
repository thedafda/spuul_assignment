'use strict';

module.exports = function(grunt) {
    return {
        prodIndex: {
            files: {
                "dist/index.html": "app/index.html",
            }
        },
        prodImage: {
            expand: true,
            cwd: 'app/imgs',
            src: '**',
            dest: 'dist/imgs',
        }
    };
};
