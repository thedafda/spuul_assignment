'use strict';

module.exports = function(grunt) {
    return {
        dev: {
            files: {
                "app/bundle/main.css": "app/**/*.less"
            }
        },
        prod: {
        	options: {
        		compress: true
        	},
        	files: {
                "dist/bundle/main.css": "app/**/*.less"
            }
        }
    };
};
