'use strict';

module.exports = function(grunt) {
    return {
    	jsx: {
	        files: ['app/components/**/*.jsx', 'app/sections/**/*.jsx'],
	        tasks: ['webpack:dev']
	    },
	    less: {
	        files: ['app/**/*.page.less', 'app/**/*.cmpnt.less'],
	        tasks: ['less:dev']
	    }
	};
};
