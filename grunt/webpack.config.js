'use strict';

let webpack = require("webpack");
let _ = require("lodash");

let defaultConfig = {
    entry: {
        home: "./app/sections/home.page",
    },
    output: {
        path: "app/bundle",
        filename: "[name].js",
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [{
            test: /\.jsx$/,
            exclude: /node_modules/,
            loader: "babel-loader",
        }]
    },
    devtool: "source-map",
    debug: true
};

let prodConfig = _.extend({}, defaultConfig, {
    output: {
        path: "dist/bundle",
        filename: "[name].js",
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ],
    debug: false
});

module.exports = {
    dev: defaultConfig,
    prod: prodConfig
};
