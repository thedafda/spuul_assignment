'use strict';

const WebPackConfig = require('./webpack.config');

module.exports = function(grunt){
	return WebPackConfig;
};